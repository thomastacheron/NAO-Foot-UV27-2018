Creation d'un compte sous GitLab --> userid
https://gitlab.com

--------------------------------------------

Creation d'un projet à partir d'un dossier local

prendre un dossier existant

création d'un fichier .gitignore pour certains fichiers ou dossiers ne soient pas gérés par git

création du git
$ git init

ajout des fichiers
$ git add .
ou  git add --all

vérification
$ git status

"commit"
$ git commit -m 'initial version'

envoi sur GitLab (en mettant les bonnes infos dans userid et project_name :
$ git push --set-upstream https://gitlab.com/userid/project_name.git master

éventuellement compléter la configuration
$ git config l

$ git config user.name "nom"
$ git config user.email "adresse email"	

--------------------------------------------

création d'une branche de travail

$ git checkout -b newbranch

vérification du changement de branche :
$ git branch
ou
$ git show-branch

faire des modifications dans la branche puis mettre à jour gitlab
$ echo "hoho" > hoho.txt
$ git add --all
$ git git commit -m  "dummy branch"

le git push ne reconnait pas la nouvelle branche ... il faut la lui indiquer :
$ git push --set-upstream origin newbranch

la branche devrait apparaitre sur gitlab (Repository-Files, ou Repository->Branch)

faire d'autres modifications dans la branche puis mettre à jour gitlab
$ echo "hoho" >> hoho.txt
$ git add --all
$ git git commit -m  "dummy branch"
$ git push

maintenant c'est ok la branche est reconnue et le push fonctionne.

si on est satisfait des modifications réalisées dans la branche , on peu la fusionner à la branche principale (le master)

on se place dans le master et on vérifie que c'est ok
$ git checkout master
$ git branch

puis on fusionne newbranch au master
$ git merge newbranch

le fichier hoho.txt est maintenant dans master, on verra plus tard comment régler les conflits si besoin (ex hoho.txt existe déjà dans master avec un contenu différent)

on peut supprimer la branche, méthode douce :
$ git branch -d newbranch
ou méthode dure :
$ git branch -D newbranch
la méthode dure est pour les branches qui n'ont pas été fusionnées ("mergées")

------------------------------------------

GitLab (à la place de GitHub) pour un gros projet


Division en équipes de 4 à 5 élèves.

Désignation des repsonsables des 4 équipes : ex Bill , nom d'utilisateur GitLab bill

Sur le navigateur, aller à l'URL suivante pour accèder au dépôt GitLab :
https://gitlab.com/bnzr/NAO-Foot-UV27-2018

et faire un fork du dépôt GitLab uniquement pour les responsables d'équipes, l'URL du dépôt GitLab devient :
https://gitlab.com/bill/NAO-Foot-UV27-2018
en remplaçant bill par votre userid

pour le namespace utiliser l'userid et définissez le dépot "forké" comme public.
Quand tout est Ok, cliquer sur "fork project"

ensuite, chaque membre de l'équipe (joe) fait un fork du projet du responsable (bill à la place de bnzr) 
https://gitlab.com/joe/NAO-Foot-UV27-2018
en remplaçant joe par votre userid GitLab

pour le namespace utiliser l'userid et définissez le dépot "forké" comme public
Quand tout est Ok, cliquer sur "fork project"


Maintenant on passe à git dans un terminal :
ouvrez un terminal et placer dans le dossier de votre choix.
cloner sur votre PC votre dépot GitLab
$ git clone https://gitlab.com/userid/NAO-Foot-UV27-2018
avec userid votre nom d'utilisateur GitLab 

$ cd NAO-Foot-UV27-2018

il faudra modifier le fichier "teams.txt"
il n'existe pas dans la branch principale "master"
$ cat teams.txt
cat: teams.txt: No such file or directory

il faut se placer dans la branche "teams" pour trouver le fichier
$ git checkout teams
$ cat teams.txt
Dummy challenge NAO Soccer 2021

Definition of the Teams

Team 1 
Name : ...
Members : ..., ..., ... 

Team 2 
Name : ... 
Members : ..., ..., ... 

Team 3 
Name :  ... 
Members : ..., ..., ... 

Team 4 
Name :  ... 
Members : ..., ..., ... 


Ensuite il faut mettre à jour ce fichier de définition des équipes avec l'éditeur de votre choix.

Selon que vous soyez membres ou responsables d'équipes vous remplacez les ... par les bonnes valeurs.

Le nom de l'équipe doit en principe être ajouté par le responsable, mais les membres peuvent faires des propositions ...

Quand c'est OK, on ajoute et valide la modification
$ git status
$ git add --all
$ git status
$ git commit -m  "inscription de joe"

si le commit ne marche pas la première fois, il faudra ajouter votre nom et votre adresse mail :
$ git config user.name "nom"
$ git config user.email "adresse email"	

et on envoie la modification vers GitLab :
$ git push

éventuellement si push ne marche pas la première fois faire :
$ git push --set-upstream origin teams

aller sur votre navigateur Web. Le commit devrait apparaitre dans GitLab et chaque membre va devoir faire un "merge request" pour que ses informations soient prises en compte par le responsable d'équipe.

sur l'interface GitLab, cliquer sur "repository" dans le menu à gauche puis sur le bouton bleu "create merge request"

lorque la page "create merge request" est à l'écran, cliquer sur "change branches" la branche cible (target branch) doit être
     bill/NAO-Foot-UV27-2018   teams
(il faut en principe juste mettre teams à la place de master et bill est l'userid de votre responsable d'équipe)
Quand tout est ok cliquer sur "compare branches and continue"

Dans "merge options" , décocher la case "Delete source branch when merge request is accepted"
Quand tout est ok cliquer sur "create merge request"

Vous aller être rediriger vers le GitLab du responsable d'équipe et ne reste plus qu'à attendre qu'il valide (ou pas) votre demande de fusion ("merge request")

Le responsable devrait voir apparaitre à cote de "Merge requests" le nombre de demandes en attente.
En cliquant sur "Merge requests" la liste des demandes apparait. Le mieux est de traiter en premier les demandes sans conflits en cliquant sur le bouton bleu "merge"
Il peut rejeter ("close merge request") une demande dont la destination serait la branche "master" au lieu de la branche "team"

Quand il y a conflit, le bouton "merge" est inactif et il faut résoudre le conflit en cliquant sur "resolve conflicts".
Le conflit est sur le fichier "teams.txt" ou il faut fusionner les memebres manuellement en cliquant sur "inline edit", puis enlever les <<<<<<<<<< , =========== et >>>>>>>>>
Quand tout est ok, on peut clique sur "Commit to source branch"

Il faut encore valider par "review" les modifications (ici le reviewer sera le responsable d'équipe) et les sauvergarder.

Normalement le bouton ble "merge" est actif et la fusion peut être effectuée.

Il reste maintenant à donner à tous les membres la dernière version de teams.txt.
Cela se nomme la synchronisation (sync ou syncing)

Pour le responsable un git pull doit être suffisant (à tester)
$ git pull

Pour les autres membres, cela se fait en deux temps : avec git , puis avec GitLab

on ajoute le dépôt mis à jour (celui du responsable) dans git
$ git remote -v
$ git remote add upstream https://gitlab.com/bnzr/NAO-Foot-UV27-2018.git
$ git remote -v
$ git fetch upstream
on vérifie qu'on est bien dans la branche teams avant de synchroniser
$ git checkout teams
$ git merge upstream/teams
$ cat teams.txt
le fichier "teams.txt" doit être dans sa dernière version

$ git remote remove upstream
$ git remote -v
$ git push

un dernier status indique que tout est "up to date"
$ git status



